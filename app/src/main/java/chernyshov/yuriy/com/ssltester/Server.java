package chernyshov.yuriy.com.ssltester;

import android.content.Context;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 24.12.14
 * Time: 10:39
 */
public class Server extends Thread {

    public static interface ServerListener {

        void onMessage(String message);

        void onConnected(int port);
    }

    private static final String CERTIFICATE_PFX = "MIIJ2QIBAzCCCZ8GCSqGSIb3DQEHAaCCCZAEggmMMIIJiDCCBD8GCSqGSIb3DQEHBqCCBDAwggQsAgEAMIIEJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIHrz5hWw9sswCAggAgIID+Pn7wCicSzU3D8kvr94KErxpNjGeVeT9b+nHzPBXjbtRiq87utOj+ZhQZHheRvSebrBPQDv4KvA2m3WO8EwccHzOgbumUAbS5poOvRmqYeezS+pnY6xMHl5E6+eEhGJhTehv+3GKrjKaLmJp/EC4JXRD0g0HCHiMPw3kfZeyG4httqf65NayjagMy9wAShU3IxguPwcVMxudxXLnkvHPJv8GIGI3qSmw+Ml7DHfPlu5NekKCyXO/xzSDESp127OX/JKThwile2DsXhTp135dN02+wEz0AjRux6koi7QEFSh1mhYFVVpbLWh0xSLYtN+gNRFxx4Z0I3bc51BM/IJoyFTYZhRp9afZqXCn5Vh1Se29Jqj5iVkJbkTqoiXwdaNY/6adXLKmLkWVAU1knPD/6CCyy4Kb6LnXdvscy+nGzN5zZFggEjs9Fa625MXOBEyGFFyGdrZmLQBh5w3vxt2OiKOvB+bMltTQBPeQvKK4dpU0tDNZdr+t9+a3m7oL6eaTI9crq+4+ntPMCr2i8kjd1gZEwpvmA8hkkTCoaMh3QkmBhE0oY/bCFK6ANQhIggFmbr5NkVUgjyCIA5ij6mazWVeCsuJVVjdbCUcNfJUOc86zXYC3rYuZDB1FUZk85JFj1mT//aR6l/zlpRiVgZqfoyH3f2HbgbxYeA1pVua+5TrcSmxwhlFEmKv8q3H6SMh1jqHPn4QTpPxaBMEaIdtuCGjjNNkbkMABRH5BluqIq/pIJDMNJrz+br2pA9s/Inqwyjz9n80dtZbFXfq/rOVhcvxhBkQ5VZItZx1z9E5QyTDOHkzMGYPDbIt7tN4BNbMcUG7UcUTxEdpdhxNIwSTcYuHhgdst+D/baOAh3tahSb/w2sXWrEll4aS6hyb0p4I5rirSWf0r+lwib62aouHLUmDmEAgfHlipK3GBblSe7rnj/W2RQbUPlmMHVr/hT6Gpm3tQZoKp75RVfMEsls481fA8wYrPUlZe8R7msYDlD8qEFDivC9yzOTTGjYywjQXBUcgOD8h2gioeulDOjgHk8M8zIeoZfuXkO67ShSnVEOrW4yg2wk6zeGQdvdfle8FQs7bSC5eJP4nbH7qcRLGuJKX06B86ug9wdbD2N9zvcZ6mZ8TNTiWwU9W683ajusBStUnR7ARWbCb76/2IYx25OAVPS7rONiImZspJK5Ca4YR1ftqw0FJbgP/q43SbtGjO8c6oB7hI1So7/wqAgcaa7z2gJaw+lTczwdqg2V8FIRjdGkAjLB01pzJtTH2Uk1v8KV+2L/HCBSMLmn6qY0FperyH8S5FAolaHKBZwefYM9FGD8kDqyc3mrjbSLr610jdj0gmSHnuhHjaMIIFQQYJKoZIhvcNAQcBoIIFMgSCBS4wggUqMIIFJgYLKoZIhvcNAQwKAQKgggTuMIIE6jAcBgoqhkiG9w0BDAEDMA4ECJsf/tZsQmOJAgIIAASCBMggl9I3VwLRslxvSmZQBm8RcxmZWclPI29FtiTq5aMKV3GKVI5o05dsNvZDOUWp8/X6HGYDhk01OtWISs6WN09GShclEUbQdD2vHvdmqCfYOQCMntanXQmuGvxCf6IAsQNx0sHalEbOed5j94eXXx4Xrw3SMoAJjqXmEmbXt0GAJamWFo2AhSGGHALVc3g27eDC0KM9uT1jTVOhMxs05SNSiNkPL0J+oOP+9HAT6Q5yv5bwK6zV/F2pQtH17g5bYOMR08r3XxuKrEv0/sAkfyciUXBvTT32yIWMZIJn+OEI+g3P7TBHppZATPETDYwECpAcbY/oNOa6gz3o7sda555WsBLhDQw/kgL33ah/ktkq7C83H/DnQPDvXiYWes4qPU9QjsUM11Va9SXiT6vOUOewr5LZIzLv9+wMRI6IYzjqQE31Kvw+EHHL9x0Q7WXLwWRtOA6LnwnuEWFYGaaDw+tsqsJgesOC6JJ3FSay8EGtBso8ja+1eaCkXzOuqazUdCBscJ9fHM18PT+RXg6bincKP6qbm2j97DtNUg7Z1wid9euipgcNs+ppYvQsTCfQhsZ2Vv08CUGKiW0/cnPANT1yp+FqwW6kEX/hgLeqI4wK8iq+paxy4/+Db+wjJvtoXS9IrAJDl6drz3waCVk+FCmtMVH0vuwvhiXzZ9ofH2dMnRvD0wvM/ycMZcOKojrJs5T6mCv5+nLCP+dEDBozWHP1g56U7hgCbQnMLTRR0uRSQ/YGrjrgGArEINzr7+xVub/NuLXvZIPGu9I9vG9s8Fm+wcQZ7lFl6LG9UyL5ud4TMgk8F4EcjBPwpdmaKdgSzN68LDWn8A8NMPfj31p7a3SZmuFsYYdBEObGLk80vCGjtAqfVRbMvxKUiZ4OAQtmuUghEMRWzV7sQObjQHRGdvaPOIv+/edOToLCvG4bJA/3EBbSkTU5N7CDJIOEiehOf6xw7rD6KB8y/n1uHjd7h4G6+NttpzhHIekiQ2ggyEi9h08qWWvYyx0rC/xL0EStmnnF3D63WMDCU7O9bGWjs5DDsHbNtXoXHleI9Q7WgKir3wE36B/8iAcetX0yYZsqUBdwQFDwS3o25AfWx2+WLmmjdT3EsMvptvClVMntrcq44BA9LGJiVtAppocUzQirzQF8BHf52Lc4GEKTH7ISA51w25ltrBqEGDdYfHrrEUUZDkhxtxm8WsfbA27xzAewKJ7drLIDWrRh6w2Aly+p93KDJ47pVRUy+TQUDr+R+XVthFrbbM3mw61iaFDnkH9tLFU1WG7bMJssu8qOtGL2NTyr6WzS4WGXGguBaeou8ouxQ4zJQ+R2QvbsgrG0CFmvvNXmeHPJ0/l3wjVjbg4HJv5Fd4W1esOgTzvhZ6WdX+BVBfDlaKPq3y+WSXHkuG9+Z1MCajdIscV+a36IfnyPXEcfGTKn6T6AlJRcslPbfj6YXSPNS3dbXGsVKMZ7yoUaHorkjLPsdFD/tWXDcsz4hhCKulC28Qz82vfzEeahW20UcnrfYHBB1x215uebRmUB8oFrZXRKcGPZjS3LI6SKJ7eIUjOhgviHBAAQkC97WtJKkkbuImVRtEZlK4q4eSBxD5HTuhddl8U0GY1LC0D/S2MU9cIXqmR3MewxJTAjBgkqhkiG9w0BCRUxFgQUmNxT6lMgO2PJxcYTHorbc+dVNsowMTAhMAkGBSsOAwIaBQAEFP+8il07ilarxKRF2uIovBBggEgQBAg4Dcx6pjYh9QICCAA=";

    private volatile boolean mIsRunning = false;

    private Socket mSocket;

    private ServerListener listener;

    private InputStream inputStream;

    private Context mContext;

    public void setContext(final Context value) {
        mContext = value;
    }

    public void stopServer() {

        mIsRunning = false;
        interrupt();

        if (inputStream != null) {
            try {
                inputStream.close();
                inputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setListener(ServerListener value) {
        listener = value;
    }

    @Override
    public synchronized void start() {

        mIsRunning = true;

        super.start();
    }

    @Override
    public void run() {

        listener.onMessage("Start Server");

        while (!isInterrupted() || mIsRunning) {
            try {
                final KeyStore keyStore = KeyStore.getInstance("PKCS12");
                final InputStream inputStream =
                        new ByteArrayInputStream(Base64.decode(CERTIFICATE_PFX, Base64.DEFAULT));
                keyStore.load(inputStream, "".toCharArray());
                final String algorithm = KeyManagerFactory.getDefaultAlgorithm();
                final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(algorithm);
                keyManagerFactory.init(keyStore, "".toCharArray());
                final SSLContext context = SSLContext.getInstance("TLSv1.2");
                context.init(keyManagerFactory.getKeyManagers(), null, null);

                final SSLServerSocket serverSocket =
                        (SSLServerSocket) context.getServerSocketFactory().createServerSocket(0);

                if (mContext != null) {
                    if (SharedPreferencesManager.getUseServerCustomCipher(mContext)) {
                        final String customServerCipherName
                                = SharedPreferencesManager.getServerCustomCipherName(mContext);
                        final String[] suits = new String[]{customServerCipherName};

                        try {
                            serverSocket.setEnabledCipherSuites(suits);
                        } catch (IllegalArgumentException e) {
                            listener.onMessage("Suit:" + customServerCipherName + ". " + e.getMessage());
                            listener.onMessage("");
                        }
                    }
                }

                final String[] enabledProtocols = serverSocket.getEnabledProtocols();
                listener.onMessage("\nEnabled protocols:");
                for (String enabledProtocol : enabledProtocols) {
                    listener.onMessage(enabledProtocol);
                }
                listener.onMessage("");

                final String[] enabledCipherSuites = serverSocket.getEnabledCipherSuites();
                listener.onMessage("Enabled cipher suites:");
                for (String enabledCipherSuite : enabledCipherSuites) {
                    listener.onMessage(enabledCipherSuite);
                }
                listener.onMessage("");

                listener.onMessage("Server running on " + serverSocket.getLocalPort());
                listener.onConnected(serverSocket.getLocalPort());

                mSocket = serverSocket.accept();

                listener.onMessage("Server socket accepted");

                readData();

                listener.onMessage("Stop Server reading");

            } catch (KeyStoreException
                    | UnrecoverableKeyException
                    | CertificateException
                    | NoSuchAlgorithmException
                    | KeyManagementException
                    | IOException e) {
                e.printStackTrace();
                listener.onMessage(e.getMessage());
            }
        }

        listener.onMessage("Stop Server");
    }

    private void readData() throws IOException {
        inputStream = mSocket.getInputStream();
        final byte[] buffer = new byte[1024];
        try {
            int i;
            while (inputStream != null && (i = inputStream.read(buffer)) != -1) {
                listener.onMessage(new String(buffer));
            }
        } catch (IOException e) {
            e.printStackTrace();
            listener.onMessage("Read data at server exception: " + e.getMessage());
        }
    }
}
