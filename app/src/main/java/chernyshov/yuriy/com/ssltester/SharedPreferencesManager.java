package chernyshov.yuriy.com.ssltester;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12.01.15
 * Time: 11:03
 */
public class SharedPreferencesManager {

    public static final String PREFS_NAME = "SSLPrefsFile";

    private static final String SERVER_CUSTOM_CIPHER = "SERVER_CUSTOM_CIPHER";
    private static final String SERVER_CUSTOM_CIPHER_DEFAULT = "SERVER_CUSTOM_CIPHER_DEFAULT";
    private static final String SERVER_CUSTOM_CIPHER_SUPPORTED = "SERVER_CUSTOM_CIPHER_SUPPORTED";
    private static final String SERVER_CUSTOM_CIPHER_NAME = "SERVER_CUSTOM_CIPHER_NAME";
    private static final String SERVER_CUSTOM_CIPHER_POSITION = "SERVER_CUSTOM_CIPHER_POSITION";
    private static final String CLIENT_CUSTOM_CIPHER = "CLIENT_CUSTOM_CIPHER";
    private static final String CLIENT_CUSTOM_CIPHER_DEFAULT = "CLIENT_CUSTOM_CIPHER_DEFAULT";
    private static final String CLIENT_CUSTOM_CIPHER_SUPPORTED = "CLIENT_CUSTOM_CIPHER_SUPPORTED";
    private static final String CLIENT_CUSTOM_CIPHER_NAME = "CLIENT_CUSTOM_CIPHER_NAME";
    private static final String CLIENT_CUSTOM_CIPHER_POSITION = "CLIENT_CUSTOM_CIPHER_POSITION";


    public static void setUseServerCustomCipher(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(SERVER_CUSTOM_CIPHER, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseServerCustomCipher(final Context context) {
        return getSharedPreferences(context).getBoolean(SERVER_CUSTOM_CIPHER, false);
    }

    public static void setUseServerCustomCipherDefault(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(SERVER_CUSTOM_CIPHER_DEFAULT, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseServerCustomCipherDefault(final Context context) {
        return getSharedPreferences(context).getBoolean(SERVER_CUSTOM_CIPHER_DEFAULT, true);
    }

    public static void setUseServerCustomCipherSupported(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(SERVER_CUSTOM_CIPHER_SUPPORTED, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseServerCustomCipherSupported(final Context context) {
        return getSharedPreferences(context).getBoolean(SERVER_CUSTOM_CIPHER_SUPPORTED, false);
    }

    public static void setServerCustomCipherName(final Context context, final String value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putString(SERVER_CUSTOM_CIPHER_NAME, value);
        // Commit the edits!
        editor.apply();
    }

    public static String getServerCustomCipherName(final Context context) {
        return getSharedPreferences(context).getString(SERVER_CUSTOM_CIPHER_NAME, "");
    }

    public static void setServerCustomCipherPosition(final Context context, final int value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putInt(SERVER_CUSTOM_CIPHER_POSITION, value);
        // Commit the edits!
        editor.apply();
    }

    public static int getServerCustomCipherPosition(final Context context) {
        return getSharedPreferences(context).getInt(SERVER_CUSTOM_CIPHER_POSITION, 0);
    }

    public static void setUseClientCustomCipher(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(CLIENT_CUSTOM_CIPHER, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseClientCustomCipher(final Context context) {
        return getSharedPreferences(context).getBoolean(CLIENT_CUSTOM_CIPHER, false);
    }

    public static void setUseClientCustomCipherDefault(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(CLIENT_CUSTOM_CIPHER_DEFAULT, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseClientCustomCipherDefault(final Context context) {
        return getSharedPreferences(context).getBoolean(CLIENT_CUSTOM_CIPHER_DEFAULT, true);
    }

    public static void setUseClientCustomCipherSupported(final Context context, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(CLIENT_CUSTOM_CIPHER_SUPPORTED, value);
        // Commit the edits!
        editor.apply();
    }

    public static boolean getUseClientCustomCipherSupported(final Context context) {
        return getSharedPreferences(context).getBoolean(CLIENT_CUSTOM_CIPHER_SUPPORTED, false);
    }

    public static void setClientCustomCipherName(final Context context, final String value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putString(CLIENT_CUSTOM_CIPHER_NAME, value);
        // Commit the edits!
        editor.apply();
    }

    public static String getClientCustomCipherName(final Context context) {
        return getSharedPreferences(context).getString(CLIENT_CUSTOM_CIPHER_NAME, "");
    }

    public static void setClientCustomCipherPosition(final Context context, final int value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putInt(CLIENT_CUSTOM_CIPHER_POSITION, value);
        // Commit the edits!
        editor.apply();
    }

    public static int getClientCustomCipherPosition(final Context context) {
        return getSharedPreferences(context).getInt(CLIENT_CUSTOM_CIPHER_POSITION, 0);
    }

    private static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0);
    }

    private static SharedPreferences.Editor getEditor(final Context context) {
        return getSharedPreferences(context).edit();
    }
}
