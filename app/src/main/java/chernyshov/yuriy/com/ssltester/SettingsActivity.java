package chernyshov.yuriy.com.ssltester;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 24.12.14
 * Time: 15:29
 */
public class SettingsActivity extends Activity {

    private static final String CLASS_NAME = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
        } catch (NoSuchAlgorithmException e) {
            Log.e(CLASS_NAME, "NoSuchAlgorithmException:" + e.getMessage());
        } catch (KeyManagementException e) {
            Log.e(CLASS_NAME, " KeyManagementException:" + e.getMessage());
        }

        String[] defaultSuites = new String[0];
        String[] supportedSuites = new String[0];
        if (sslContext != null) {
            defaultSuites = sslContext.getSocketFactory().getDefaultCipherSuites();
            supportedSuites = sslContext.getSocketFactory().getSupportedCipherSuites();
        }

        final ArrayAdapter<String> defaultCiphersAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, defaultSuites);
        defaultCiphersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<String> supportedCiphersAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, supportedSuites);
        supportedCiphersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        /**
         * Server
         */

        final CheckBox serverEnableCustomCipherCheckView =
                (CheckBox) findViewById(R.id.server_custom_cipher_check_view);
        final RadioButton serverDefaultCipherRadioView =
                (RadioButton) findViewById(R.id.server_default_cipher_radio_view);
        final RadioButton serverSupportedCipherRadioView =
                (RadioButton) findViewById(R.id.server_supported_cipher_radio_view);
        final Spinner serverDefaultCiphersView =
                (Spinner) findViewById(R.id.server_default_ciphers_spinner_view);
        final Spinner serverSupportedCiphersView =
                (Spinner) findViewById(R.id.server_supported_ciphers_spinner_view);

        serverDefaultCiphersView.setAdapter(defaultCiphersAdapter);
        serverSupportedCiphersView.setAdapter(supportedCiphersAdapter);

        serverEnableCustomCipherCheckView.setChecked(SharedPreferencesManager.getUseServerCustomCipher(this));
        serverEnableCustomCipherCheckView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        processEnabledServerCustomCipherCheckView(isChecked);
                    }
                }
        );
        processEnabledServerCustomCipherCheckView(serverEnableCustomCipherCheckView.isChecked());

        serverDefaultCipherRadioView.setChecked(
                SharedPreferencesManager.getUseServerCustomCipherDefault(this)
        );
        serverDefaultCipherRadioView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        serverDefaultCiphersView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
        );

        serverSupportedCipherRadioView.setChecked(
                SharedPreferencesManager.getUseServerCustomCipherSupported(this)
        );
        serverSupportedCipherRadioView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        serverSupportedCiphersView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
        );

        serverDefaultCiphersView.setSelection(
                SharedPreferencesManager.getServerCustomCipherPosition(this)
        );
        serverDefaultCiphersView.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        final String serverCipherName = defaultCiphersAdapter.getItem(position);
                        Log.d(CLASS_NAME, "Selected Server cipher name:" + serverCipherName);
                        SharedPreferencesManager.setServerCustomCipherName(
                                SettingsActivity.this, serverCipherName
                        );
                        SharedPreferencesManager.setServerCustomCipherPosition(
                                SettingsActivity.this, position
                        );
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        /**
         * Client
         */

        final CheckBox clientEnableCustomCipherCheckView =
                (CheckBox) findViewById(R.id.client_custom_cipher_check_view);
        final RadioButton clientDefaultCipherRadioView =
                (RadioButton) findViewById(R.id.client_default_cipher_radio_view);
        final RadioButton clientSupportedCipherRadioView =
                (RadioButton) findViewById(R.id.client_supported_cipher_radio_view);
        final Spinner clientDefaultCiphersView =
                (Spinner) findViewById(R.id.client_default_ciphers_spinner_view);
        final Spinner clientSupportedCiphersView =
                (Spinner) findViewById(R.id.client_supported_ciphers_spinner_view);

        clientDefaultCiphersView.setAdapter(defaultCiphersAdapter);
        clientSupportedCiphersView.setAdapter(supportedCiphersAdapter);

        clientEnableCustomCipherCheckView.setChecked(SharedPreferencesManager.getUseClientCustomCipher(this));
        clientEnableCustomCipherCheckView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        processEnabledClientCustomCipherCheckView(isChecked);
                    }
                }
        );
        processEnabledClientCustomCipherCheckView(clientEnableCustomCipherCheckView.isChecked());

        clientDefaultCipherRadioView.setChecked(
                SharedPreferencesManager.getUseClientCustomCipherDefault(this)
        );
        clientDefaultCipherRadioView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        clientDefaultCiphersView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
        );

        clientSupportedCipherRadioView.setChecked(
                SharedPreferencesManager.getUseClientCustomCipherSupported(this)
        );
        clientSupportedCipherRadioView.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        clientSupportedCiphersView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    }
                }
        );

        clientDefaultCiphersView.setSelection(
                SharedPreferencesManager.getServerCustomCipherPosition(this)
        );
        clientDefaultCiphersView.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        final String clientCipherName = defaultCiphersAdapter.getItem(position);
                        Log.d(CLASS_NAME, "Client cipher name:" + clientCipherName);
                        SharedPreferencesManager.setClientCustomCipherName(
                                SettingsActivity.this, clientCipherName
                        );
                        SharedPreferencesManager.setClientCustomCipherPosition(
                                SettingsActivity.this, position
                        );
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveRadioButtonsStates();
    }

    public static Intent makeIntent(final Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    private void saveRadioButtonsStates() {
        final RadioButton serverDefaultCipherRadioView =
                (RadioButton) findViewById(R.id.server_default_cipher_radio_view);
        final RadioButton serverSupportedCipherRadioView =
                (RadioButton) findViewById(R.id.server_supported_cipher_radio_view);
        final RadioButton clientDefaultCipherRadioView =
                (RadioButton) findViewById(R.id.client_default_cipher_radio_view);
        final RadioButton clientSupportedCipherRadioView =
                (RadioButton) findViewById(R.id.client_supported_cipher_radio_view);

        SharedPreferencesManager.setUseClientCustomCipherDefault(
                this, clientDefaultCipherRadioView.isChecked()
        );
        SharedPreferencesManager.setUseClientCustomCipherSupported(
                this, clientSupportedCipherRadioView.isChecked()
        );

        SharedPreferencesManager.setUseServerCustomCipherDefault(
                this, serverDefaultCipherRadioView.isChecked()
        );
        SharedPreferencesManager.setUseServerCustomCipherSupported(
                this, serverSupportedCipherRadioView.isChecked()
        );
    }

    private void processEnabledServerCustomCipherCheckView(boolean isEnabled) {
        final Spinner defaultCiphersView =
                (Spinner) findViewById(R.id.server_default_ciphers_spinner_view);
        final Spinner supportedCiphersView =
                (Spinner) findViewById(R.id.server_supported_ciphers_spinner_view);
        final RadioButton defaultCipherRadioView =
                (RadioButton) findViewById(R.id.server_default_cipher_radio_view);
        final RadioButton supportedCipherRadioView =
                (RadioButton) findViewById(R.id.server_supported_cipher_radio_view);

        defaultCiphersView.setEnabled(isEnabled);
        supportedCiphersView.setEnabled(isEnabled);
        defaultCipherRadioView.setEnabled(isEnabled);
        supportedCipherRadioView.setEnabled(isEnabled);

        SharedPreferencesManager.setUseServerCustomCipher(this, isEnabled);
    }

    private void processEnabledClientCustomCipherCheckView(boolean isEnabled) {
        final Spinner defaultCiphersView =
                (Spinner) findViewById(R.id.client_default_ciphers_spinner_view);
        final Spinner supportedCiphersView =
                (Spinner) findViewById(R.id.client_supported_ciphers_spinner_view);
        final RadioButton defaultCipherRadioView =
                (RadioButton) findViewById(R.id.client_default_cipher_radio_view);
        final RadioButton supportedCipherRadioView =
                (RadioButton) findViewById(R.id.client_supported_cipher_radio_view);

        defaultCiphersView.setEnabled(isEnabled);
        supportedCiphersView.setEnabled(isEnabled);
        defaultCipherRadioView.setEnabled(isEnabled);
        supportedCipherRadioView.setEnabled(isEnabled);

        SharedPreferencesManager.setUseClientCustomCipher(this, isEnabled);
    }
}
