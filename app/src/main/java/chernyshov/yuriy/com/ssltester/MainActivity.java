package chernyshov.yuriy.com.ssltester;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends ActionBarActivity {

    private static final String CLASS_NAME = MainActivity.class.getSimpleName();

    private Server server;
    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(SettingsActivity.makeIntent(this));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopServer();
    }

    public void onStartServerClickHandler(final View view) {
        startServer();
    }

    public void onStopServerClickHandler(final View view) {
        stopServer();
    }

    public void onClientMessageClickHandler(final View view) {
        if (client == null) {
            return;
        }
        final String message
                = ((EditText) findViewById(R.id.client_edit_text_view)).getText().toString();
        try {
            client.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startServer() {
        server = new Server();
        server.setContext(this);
        server.setListener(
                new Server.ServerListener() {

                    @Override
                    public void onMessage(final String message) {

                        updateMessageFromServer(message);
                    }

                    @Override
                    public void onConnected(int port) {
                        startClient(port);
                    }
                }
        );
        server.start();
    }

    private void updateMessageFromServer(final String message) {
        Log.d(CLASS_NAME, "SERVER: " + message);

        runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        final TextView textView = (TextView) findViewById(R.id.server_messages_view);
                        textView.append("\n" + message);

                        final ScrollView scrollview
                                = ((ScrollView) findViewById(R.id.server_messages_scroll_view));
                        scrollview.post(new Runnable() {

                            @Override
                            public void run() {
                                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
                    }
                }
        );
    }

    private void stopServer() {
        if (server == null) {
            return;
        }

        Thread thread = new Thread(
                new Runnable() {

                    @Override
                    public void run() {
                        Log.i(CLASS_NAME, "Stopping server ...");
                        server.stopServer();
                    }
                }
        );
        thread.start();

    }

    private void startClient(int port) {
        client = new Client();
        client.setPort(port);
        client.setContext(this);
        client.setListener(
                new Client.ClientListener() {

                    @Override
                    public void onMessage(final String message) {

                        updateMessageFromClient(message);

                    }
                }
        );
        client.start();
    }

    private void updateMessageFromClient(final String message) {
        Log.d(CLASS_NAME, "CLIENT: " + message);

        runOnUiThread(
                new Runnable() {

                    @Override
                    public void run() {
                        final TextView textView = (TextView) findViewById(R.id.client_messages_view);
                        textView.append("\n" + message);

                        final ScrollView scrollview
                                = ((ScrollView) findViewById(R.id.client_messages_scroll_view));
                        scrollview.post(new Runnable() {

                            @Override
                            public void run() {
                                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
                    }
                }
        );
    }
}
