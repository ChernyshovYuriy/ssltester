package chernyshov.yuriy.com.ssltester;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.SocketFactory;
import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 24.12.14
 * Time: 12:22
 */
public class Client extends Thread {

    public static interface ClientListener {

        void onMessage(String message);
    }

    private static final String CLASS_NAME = Client.class.getSimpleName();

    private int mPort;

    private ClientListener listener;

    private BufferedWriter bufferedWriter;

    private Context mContext;

    public void setContext(final Context value) {
        mContext = value;
    }

    public void setPort(int value) {
        mPort = value;
    }

    public void setListener(ClientListener value) {
        listener = value;
    }

    @Override
    public void run() {

        try {

            listener.onMessage("Start client");

            final TrustManager[] trustAllCerts = createTrustManagers();
            final SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            final SocketFactory socketFactory = sslContext.getSocketFactory();
            final InetAddress inetAddress = InetAddress.getByName("127.0.0.1");

            final SSLSocket sslSocket = (SSLSocket) socketFactory.createSocket(inetAddress, mPort);

            final String[] enabledProtocols = sslSocket.getEnabledProtocols();
            listener.onMessage("\nEnabled protocols:");
            for (String enabledProtocol : enabledProtocols) {
                listener.onMessage(enabledProtocol);
            }
            listener.onMessage("");

            if (mContext != null) {
                if (SharedPreferencesManager.getUseClientCustomCipher(mContext)) {
                    final String customServerCipherName
                            = SharedPreferencesManager.getClientCustomCipherName(mContext);
                    final String[] suits = new String[]{customServerCipherName};

                    try {
                        sslSocket.setEnabledCipherSuites(suits);
                    } catch (IllegalArgumentException e) {
                        listener.onMessage("Suit:" + customServerCipherName + ". " + e.getMessage());
                        listener.onMessage("");
                    }
                }
            }

            final String[] enabledCipherSuites = sslSocket.getEnabledCipherSuites();
            listener.onMessage("Enabled cipher suites:");
            for (String enabledCipherSuite : enabledCipherSuites) {
                listener.onMessage(enabledCipherSuite);
            }
            listener.onMessage("");

            sslSocket.addHandshakeCompletedListener(
                    new HandshakeCompletedListener() {

                        @Override
                        public void handshakeCompleted(HandshakeCompletedEvent event) {
                            listener.onMessage("Handshake complete");
                        }
                    }
            );
            sslSocket.startHandshake();

            bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(sslSocket.getOutputStream()));
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(sslSocket.getInputStream()));

            String string;

            while ((string = bufferedReader.readLine())!= null) {

                listener.onMessage("Read data at client");

                bufferedWriter.write(string, 0, string.length());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }

            bufferedWriter.close();
            bufferedReader.close();
            sslSocket.close();
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException exception) {
            exception.printStackTrace();
            listener.onMessage("Client exception: " + exception.getMessage());
        }

        listener.onMessage("Stop Client");
    }

    public void sendMessage(final String message) throws IOException {
        if (Thread.currentThread().getName().equalsIgnoreCase("main")) {

            final Thread thread = new Thread(
                    new Runnable() {

                        @Override
                        public void run() {
                            try {
                                doSendMessage(message);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            );
            thread.start();

            return;
        }

        doSendMessage(message);
    }

    private void doSendMessage(final String message) throws IOException {
        bufferedWriter.newLine();
        bufferedWriter.write(message, 0, message.length());
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    private TrustManager[] createTrustManagers() {

        // Create a trust manager that does not validate certificate chains
        return new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        }};
    }
}
